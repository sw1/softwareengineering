package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CameraChooserWindow extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public CameraChooserWindow() {
		setTitle("Wybór kamery");
		setBounds(100, 100, 940, 635);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblKliknijNaKamer = new JLabel("Kliknij na kamerę");
		GridBagConstraints gbc_lblKliknijNaKamer = new GridBagConstraints();
		gbc_lblKliknijNaKamer.insets = new Insets(0, 0, 5, 5);
		gbc_lblKliknijNaKamer.gridx = 0;
		gbc_lblKliknijNaKamer.gridy = 0;
		contentPane.add(lblKliknijNaKamer, gbc_lblKliknijNaKamer);
		
		JLabel label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame camWin=new CameraWindow();
				camWin.setVisible(true);
			}
		});
		label.setIcon(new ImageIcon(CameraChooserWindow.class.getResource("/gui/mapa_kamer.jpg")));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		contentPane.add(label, gbc_label);
		
		JList list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame camWin=new CameraWindow();
				camWin.setVisible(true);
			}
		});
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"droga A4 15km", "droga A4 25km", "ulica Czarnowiejska, Kraków", "ulica Nawojki, Kraków", "ulica Armii Krajowej, Kraków", "droga 94 60km"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.insets = new Insets(0, 0, 0, 5);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 1;
		contentPane.add(list, gbc_list);
	}

}
