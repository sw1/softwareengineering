package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JList;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;

public class CameraWindow extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CameraWindow frame = new CameraWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CameraWindow() {
		setTitle("Widok z kamery");
		setBounds(100, 100, 810, 545);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblRozpoznaneSamochody = new JLabel("Rozpoznane Samochody");
		GridBagConstraints gbc_lblRozpoznaneSamochody = new GridBagConstraints();
		gbc_lblRozpoznaneSamochody.insets = new Insets(0, 0, 5, 5);
		gbc_lblRozpoznaneSamochody.gridx = 0;
		gbc_lblRozpoznaneSamochody.gridy = 0;
		contentPane.add(lblRozpoznaneSamochody, gbc_lblRozpoznaneSamochody);
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"KR81023", "KT5494S", "KOS1567", "WA942SA", "KRA45913"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.insets = new Insets(0, 0, 5, 5);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 1;
		contentPane.add(list, gbc_list);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(CameraWindow.class.getResource("/gui/obraz_kamery.jpg")));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		JButton btnled = new JButton("Śledź");
		GridBagConstraints gbc_btnled = new GridBagConstraints();
		gbc_btnled.insets = new Insets(0, 0, 0, 5);
		gbc_btnled.gridx = 0;
		gbc_btnled.gridy = 2;
		contentPane.add(btnled, gbc_btnled);
	}

}
