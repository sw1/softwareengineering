package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow {

	private JFrame frmMonitoringDrg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmMonitoringDrg.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMonitoringDrg = new JFrame();
		frmMonitoringDrg.setTitle("Monitoring dróg");
		frmMonitoringDrg.setBounds(100, 100, 655, 435);
		frmMonitoringDrg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{270, 147, 0, 0};
		gridBagLayout.rowHeights = new int[] {30, 30, 30, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		frmMonitoringDrg.getContentPane().setLayout(gridBagLayout);
		
		JButton btnNewButton = new JButton("Znajdź pojazd");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame fdnWin= new FindingWindow();
				fdnWin.setVisible(true);
			}
		});

		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		frmMonitoringDrg.getContentPane().add(btnNewButton, gbc_btnNewButton);
		
		JLabel lblledzoneSamochody = new JLabel("Śledzone Samochody");
		GridBagConstraints gbc_lblledzoneSamochody = new GridBagConstraints();
		gbc_lblledzoneSamochody.gridwidth = 2;
		gbc_lblledzoneSamochody.insets = new Insets(0, 0, 5, 0);
		gbc_lblledzoneSamochody.gridx = 1;
		gbc_lblledzoneSamochody.gridy = 0;
		frmMonitoringDrg.getContentPane().add(lblledzoneSamochody, gbc_lblledzoneSamochody);
		
		JButton btnMonitoruj = new JButton("Monitoruj drogi");
		btnMonitoruj.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame camChWin=new CameraChooserWindow();
				camChWin.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMonitoruj = new GridBagConstraints();
		gbc_btnMonitoruj.insets = new Insets(0, 0, 5, 5);
		gbc_btnMonitoruj.gridx = 0;
		gbc_btnMonitoruj.gridy = 1;
		frmMonitoringDrg.getContentPane().add(btnMonitoruj, gbc_btnMonitoruj);
		
		JButton btnDodaj = new JButton("Dodaj");
		GridBagConstraints gbc_btnDodaj = new GridBagConstraints();
		gbc_btnDodaj.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodaj.gridx = 1;
		gbc_btnDodaj.gridy = 1;
		frmMonitoringDrg.getContentPane().add(btnDodaj, gbc_btnDodaj);
		
		JButton btnUsu = new JButton("Usuń");
		GridBagConstraints gbc_btnUsu = new GridBagConstraints();
		gbc_btnUsu.insets = new Insets(0, 0, 5, 0);
		gbc_btnUsu.gridx = 2;
		gbc_btnUsu.gridy = 1;
		frmMonitoringDrg.getContentPane().add(btnUsu, gbc_btnUsu);
		
		JList list_1 = new JList();
		list_1.setModel(new AbstractListModel() {
			String[] values = new String[] {"KR56891", "KT91243", "KZ54796", "KRA7892S", "KR75513"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		GridBagConstraints gbc_list_1 = new GridBagConstraints();
		gbc_list_1.gridwidth = 2;
		gbc_list_1.gridheight = 4;
		gbc_list_1.fill = GridBagConstraints.BOTH;
		gbc_list_1.gridx = 1;
		gbc_list_1.gridy = 2;
		frmMonitoringDrg.getContentPane().add(list_1, gbc_list_1);
		
		JLabel lblNewLabel = new JLabel("Powiadomienia");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 2;
		frmMonitoringDrg.getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"KR81070 na ulicy Bora-Komorowskiego w Krakowie", "KSW90151 na drodze nr 52 w okolicach Suchej", "KT24S51 na drodze nr A4 w okolicach Bochni"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.insets = new Insets(0, 0, 5, 5);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 3;
		frmMonitoringDrg.getContentPane().add(list, gbc_list);
		
		JButton btnSzczegy = new JButton("Szczegóły");
		btnSzczegy.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame dtlWin=new DetailsWindow();
				dtlWin.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnSzczegy = new GridBagConstraints();
		gbc_btnSzczegy.insets = new Insets(0, 0, 0, 5);
		gbc_btnSzczegy.gridx = 0;
		gbc_btnSzczegy.gridy = 4;
		frmMonitoringDrg.getContentPane().add(btnSzczegy, gbc_btnSzczegy);
	}

}
