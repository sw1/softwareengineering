package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataListener;

import data.CarData;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class FindingWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JList<String> list;
	private CarData db = new CarData();

	/**
	 * Create the frame.
	 */
	public FindingWindow() {
		setTitle("Szukanie");
		setBounds(100, 100, 566, 529);
		contentPane = new JPanel();
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblNrRejestracyjny = new JLabel("Nr rejestracyjny:");
		GridBagConstraints gbc_lblNrRejestracyjny = new GridBagConstraints();
		gbc_lblNrRejestracyjny.insets = new Insets(0, 0, 5, 0);
		gbc_lblNrRejestracyjny.gridx = 0;
		gbc_lblNrRejestracyjny.gridy = 0;
		contentPane.add(lblNrRejestracyjny, gbc_lblNrRejestracyjny);

		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);

		JButton btnSzukaj = new JButton("Szukaj!");
		btnSzukaj.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String regNum = textField.getText().replaceAll("[ -/]", "");
				final List<String> listData = db.getData(regNum);
				list.setModel(new AbstractListModel<String>() {

					@Override
					public String getElementAt(int index) {
						// TODO Auto-generated method stub
						return listData.get(index);
					}

					@Override
					public int getSize() {
						// TODO Auto-generated method stub
						return listData.size();
					}
				});

				if (listData.size() == 0) {
					JOptionPane.showMessageDialog(contentPane,
							"Nie znaleziono samochodu");
				} else {
					String registration=listData.get(0).split(" ")[0];
					Object[] options = { "Tak", "Nie"};
					int n = JOptionPane.showOptionDialog(contentPane,
							"Czy śledzić pojazd "+registration+"?", "Pytanie o śledzenie "+registration,
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE, null, options,
							options[0]);
					JFrame dtlWin=new DetailsWindow();
					dtlWin.setVisible(true);
				}
			}
		});
		GridBagConstraints gbc_btnSzukaj = new GridBagConstraints();
		gbc_btnSzukaj.insets = new Insets(0, 0, 5, 0);
		gbc_btnSzukaj.gridx = 0;
		gbc_btnSzukaj.gridy = 2;
		contentPane.add(btnSzukaj, gbc_btnSzukaj);

		JLabel lblZnalezione = new JLabel("Znalezione:");
		GridBagConstraints gbc_lblZnalezione = new GridBagConstraints();
		gbc_lblZnalezione.insets = new Insets(0, 0, 5, 0);
		gbc_lblZnalezione.gridx = 0;
		gbc_lblZnalezione.gridy = 3;
		contentPane.add(lblZnalezione, gbc_lblZnalezione);

		list = new JList();
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 4;
		contentPane.add(list, gbc_list);
	}

}
