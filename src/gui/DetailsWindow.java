package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class DetailsWindow extends JFrame {

	private JPanel contentPane;
	private JTextField txtKr;
	private JTextField txtUlicaCzarnowiesjkaKrakw;
	private JTextField txtJakubKowal;
	private JTextField txtOpelAstra;
	private JTextField txtSrebrny;
	private JLabel lblNewLabel_2;
	private JLabel lblData;
	private JTextField textField;
	private JLabel lblWsprzdne;
	private JTextField textField_1;
	private JButton btnPowiksz;
	private JButton btnPomniejsz;

	/**
	 * Create the frame.
	 */
	public DetailsWindow() {
		setTitle("Szczegóły");
		setBounds(100, 100, 1024, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 209, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 2.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblNewLabel = new JLabel("Nr rejestracyjny");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		txtKr = new JTextField();
		txtKr.setText("KR824731");
		txtKr.setEditable(false);
		GridBagConstraints gbc_txtKr = new GridBagConstraints();
		gbc_txtKr.insets = new Insets(0, 0, 5, 5);
		gbc_txtKr.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKr.gridx = 1;
		gbc_txtKr.gridy = 0;
		contentPane.add(txtKr, gbc_txtKr);
		txtKr.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Mapa");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 0;
		contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JLabel lblLokalizacja = new JLabel("Lokalizacja");
		GridBagConstraints gbc_lblLokalizacja = new GridBagConstraints();
		gbc_lblLokalizacja.insets = new Insets(0, 0, 5, 5);
		gbc_lblLokalizacja.gridx = 0;
		gbc_lblLokalizacja.gridy = 1;
		contentPane.add(lblLokalizacja, gbc_lblLokalizacja);
		
		txtUlicaCzarnowiesjkaKrakw = new JTextField();
		txtUlicaCzarnowiesjkaKrakw.setText("ulica Czarnowiesjka, Kraków");
		txtUlicaCzarnowiesjkaKrakw.setEditable(false);
		GridBagConstraints gbc_txtUlicaCzarnowiesjkaKrakw = new GridBagConstraints();
		gbc_txtUlicaCzarnowiesjkaKrakw.insets = new Insets(0, 0, 5, 5);
		gbc_txtUlicaCzarnowiesjkaKrakw.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUlicaCzarnowiesjkaKrakw.gridx = 1;
		gbc_txtUlicaCzarnowiesjkaKrakw.gridy = 1;
		contentPane.add(txtUlicaCzarnowiesjkaKrakw, gbc_txtUlicaCzarnowiesjkaKrakw);
		txtUlicaCzarnowiesjkaKrakw.setColumns(10);
		
		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(DetailsWindow.class.getResource("/gui/map.png")));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.gridwidth = 3;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridheight = 7;
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 1;
		contentPane.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblWaciciel = new JLabel("Właściciel");
		GridBagConstraints gbc_lblWaciciel = new GridBagConstraints();
		gbc_lblWaciciel.insets = new Insets(0, 0, 5, 5);
		gbc_lblWaciciel.gridx = 0;
		gbc_lblWaciciel.gridy = 2;
		contentPane.add(lblWaciciel, gbc_lblWaciciel);
		
		txtJakubKowal = new JTextField();
		txtJakubKowal.setText("Jakub Kowal");
		txtJakubKowal.setEditable(false);
		GridBagConstraints gbc_txtJakubKowal = new GridBagConstraints();
		gbc_txtJakubKowal.insets = new Insets(0, 0, 5, 5);
		gbc_txtJakubKowal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtJakubKowal.gridx = 1;
		gbc_txtJakubKowal.gridy = 2;
		contentPane.add(txtJakubKowal, gbc_txtJakubKowal);
		txtJakubKowal.setColumns(10);
		
		JLabel lblMarka = new JLabel("Marka");
		GridBagConstraints gbc_lblMarka = new GridBagConstraints();
		gbc_lblMarka.insets = new Insets(0, 0, 5, 5);
		gbc_lblMarka.gridx = 0;
		gbc_lblMarka.gridy = 3;
		contentPane.add(lblMarka, gbc_lblMarka);
		
		txtOpelAstra = new JTextField();
		txtOpelAstra.setText("Opel Astra");
		txtOpelAstra.setEditable(false);
		GridBagConstraints gbc_txtOpelAstra = new GridBagConstraints();
		gbc_txtOpelAstra.insets = new Insets(0, 0, 5, 5);
		gbc_txtOpelAstra.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOpelAstra.gridx = 1;
		gbc_txtOpelAstra.gridy = 3;
		contentPane.add(txtOpelAstra, gbc_txtOpelAstra);
		txtOpelAstra.setColumns(10);
		
		JLabel lblKolor = new JLabel("Kolor");
		GridBagConstraints gbc_lblKolor = new GridBagConstraints();
		gbc_lblKolor.insets = new Insets(0, 0, 5, 5);
		gbc_lblKolor.gridx = 0;
		gbc_lblKolor.gridy = 4;
		contentPane.add(lblKolor, gbc_lblKolor);
		
		txtSrebrny = new JTextField();
		txtSrebrny.setText("Srebrny");
		txtSrebrny.setEditable(false);
		GridBagConstraints gbc_txtSrebrny = new GridBagConstraints();
		gbc_txtSrebrny.insets = new Insets(0, 0, 5, 5);
		gbc_txtSrebrny.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSrebrny.gridx = 1;
		gbc_txtSrebrny.gridy = 4;
		contentPane.add(txtSrebrny, gbc_txtSrebrny);
		txtSrebrny.setColumns(10);
		
		lblData = new JLabel("Data");
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.anchor = GridBagConstraints.NORTH;
		gbc_lblData.insets = new Insets(0, 0, 5, 5);
		gbc_lblData.gridx = 0;
		gbc_lblData.gridy = 5;
		contentPane.add(lblData, gbc_lblData);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setText("16:21:15 04-12-2014");
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.NORTH;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 5;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);
		
		lblWsprzdne = new JLabel("Współrzędne");
		GridBagConstraints gbc_lblWsprzdne = new GridBagConstraints();
		gbc_lblWsprzdne.anchor = GridBagConstraints.NORTH;
		gbc_lblWsprzdne.insets = new Insets(0, 0, 5, 5);
		gbc_lblWsprzdne.gridx = 0;
		gbc_lblWsprzdne.gridy = 6;
		contentPane.add(lblWsprzdne, gbc_lblWsprzdne);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setText("50.0667, 19.9181");
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.NORTH;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 6;
		contentPane.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		btnPowiksz = new JButton("Powiększ");
		GridBagConstraints gbc_btnPowiksz = new GridBagConstraints();
		gbc_btnPowiksz.insets = new Insets(0, 0, 0, 5);
		gbc_btnPowiksz.gridx = 3;
		gbc_btnPowiksz.gridy = 8;
		contentPane.add(btnPowiksz, gbc_btnPowiksz);
		
		btnPomniejsz = new JButton("Pomniejsz");
		GridBagConstraints gbc_btnPomniejsz = new GridBagConstraints();
		gbc_btnPomniejsz.gridx = 4;
		gbc_btnPomniejsz.gridy = 8;
		contentPane.add(btnPomniejsz, gbc_btnPomniejsz);
	}

}
