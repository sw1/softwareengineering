package data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CarData {

	ArrayList<String> data=new ArrayList<String>();

	public CarData(){
		data.add("KR59141 ulica Bora Komorowskiego, Kraków");
		data.add("KR59141 ulica Al. 29 Listopada, Kraków");
		data.add("KR82912 ulica Zakopiańska, Kraków");
		data.add("KR82912 droga A4 15km");
		data.add("KR82912 droga A4 25km");
		data.add("KT42334 ulica Tuchowska, Okrężna");
		data.add("KT42334 ulica Okrężna, Okrężna");
		data.add("KOS3542 ulica Olszewskiego, Oświęcim");
		data.add("KWI5486 droga 94");
	}
	
	public List<String> getData(String regNum){
		List<String> result=new ArrayList<String>();
		for(String reg:data){
			if(reg.contains(regNum)){
				result.add(reg);
			}
		}
		return result;
	}
}
